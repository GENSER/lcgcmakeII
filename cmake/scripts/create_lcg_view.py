#!/usr/bin/env python

# Author: Ivan Razumov
# Usage: lcgview.py [options] <view_root>                                                                                                           
# This package creates a "view" of LCG release in folder <view_root>.
# optional arguments:
#    -h, --help                       show this help message and exit
#    -p LCGPATH, --lcgpath LCGPATH    top directory of LCG releases (default: /afs/cern.ch/sw/lcg/releases)
#    -r LCGREL, --release LCGREL      LCG release number (default: 80)
#    -l LCGPLAT, --platform LCGPLAT   Platform to use (default: x86_64-slc6-gcc49-opt)
#    -d, --delete                     delete old view before creating a new one
#    -v, --version                    show program's version number and exit
#    -B, --enable-blacklists          Enable built-in blacklists of files and packages. For experts only.
#    
	
import argparse
import os
import sys
import shutil
import re
import time
import glob


def get_externals_from_release_file(lcg_file):
    externals = {}
    txt_file = open(lcg_file)
    for line in txt_file.readlines():
        try:
            name, pkghash, version, home, deps = [x.strip() for x in line.split(';')]
        except ValueError:
            continue
        else:
            r = {'NAME': name, 'HASH': pkghash, 'HOME': home,
                 'VERSION': version}
            externals.update({name: r})

    return externals

def splitall(path):
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts
    
def get_externals_from_directory(directory, platform):
    dirs = glob.glob(os.path.join(directory, '*/*', platform)) + glob.glob(os.path.join(directory, '*/*/*', platform))
    externals = {}
    for d in dirs :
        p = splitall(d) 
        r = {'NAME':p[-3], 'VERSION':p[-2], 'HOME':d.replace(directory,'.')}
        externals.update({p[-3]:r})
    return externals


def write_setup(lcg_root, view_root,  platform):
    arch, osvers, compvers, buildtype = platform.split('-')
    patt = re.compile('([a-z]+)([0-9]+)')
    mobj = patt.match(compvers)
    compiler = mobj.group(1)
    version = '.'.join(list(mobj.group(2)))
    if os.path.exists(os.path.normpath(os.path.join(lcg_root,'..','contrib',compiler))):
        compiler = os.path.normpath(os.path.join(lcg_root,'..','contrib',compiler,version,'-'.join((arch, osvers))))
    elif os.path.exists(os.path.normpath(os.path.join(lcg_root,'../../../..','contrib',compiler))):
        compiler = os.path.normpath(os.path.join(lcg_root,'../../../..','contrib',compiler,version,'-'.join((arch, osvers))))
    setup_sh = """#---Source this script to setup the complete environment for this LCG view
#   Generated: @date@
#---Get the location this script (thisdir)
thisdir=$(dirname ${BASH_ARGV[0]})
#  First the compiler
source @compilerlocation@/setup.sh
CC=`which gcc`; export CC
CXX=`which g++`; export CXX
FC=`which gfortran`; export FC
#  then the rest...
if [ -z "${PATH}" ]; then
   PATH=${thisdir}/bin; export PATH
else
   PATH=${thisdir}/bin:$PATH; export PATH
fi
if [ -z "${LD_LIBRARY_PATH}" ]; then
   LD_LIBRARY_PATH=${thisdir}/lib64:${thisdir}/lib; export LD_LIBRARY_PATH
else
   LD_LIBRARY_PATH=${thisdir}/lib64:${thisdir}/lib:$LD_LIBRARY_PATH; export LD_LIBRARY_PATH
fi
if [ -z "${PYTHONPATH}" ]; then
   PYTHONPATH=${thisdir}/lib:${thisdir}/lib/python2.7/site-packages; export PYTHONPATH
else
   PYTHONPATH=${thisdir}/lib:${thisdir}/lib/python2.7/site-packages:$PYTHONPATH; export PYTHONPATH
fi
if [ -z "${MANPATH}" ]; then
   MANPATH=${thisdir}/man; export MANPATH
else
   MANPATH=${thisdir}/man:$MANPATH; export MANPATH
fi
if [ -z "${CMAKE_PREFIX_PATH}" ]; then
   CMAKE_PREFIX_PATH=${thisdir}; export CMAKE_PREFIX_PATH
else
   CMAKE_PREFIX_PATH=${thisdir}:$CMAKE_PREFIX_PATH; export CMAKE_PREFIX_PATH
fi
#---then ROOT
if [ -x $thisdir/bin/root ]; then
   ROOTSYS=$(dirname $(dirname $(readlink $thisdir/bin/root))); export ROOTSYS
   if [ -z "${ROOT_INCLUDE_PATH}" ]; then
      ROOT_INCLUDE_PATH=${thisdir}/include; export ROOT_INCLUDE_PATH
   else
      ROOT_INCLUDE_PATH=${thisdir}/include:$ROOT_INCLUDE_PATH; export ROOT_INCLUDE_PATH
   fi
fi
"""

    setup_csh = """#---Source this script to setup the complete environment for this LCG view
#   Generated: @date@
 #---Get the location this script (thisdir)
set ARGS=($_)
set LSOF=`env PATH=/usr/sbin:${PATH} which lsof`
set thisfile="`${LSOF} -w +p $$ | grep -oE '/.*setup.csh'  `"
if ( "$thisfile" != "" && -e ${thisfile} ) then
   set thisdir="`dirname ${thisfile}`"
else if ("$ARGS" != "") then
   set thisdir="`dirname ${ARGS[2]}`"
endif
#---First the compiler
source @compilerlocation@/setup.csh 
setenv CC `which gcc`
setenv CXX `which g++`
setenv FC `which gfortran`
#---then the rest...
if ($?PATH) then
   setenv PATH ${thisdir}/bin:${PATH}
else
   setenv PATH ${thisdir}/bin
endif
if ($?LD_LIBRARY_PATH) then
   setenv LD_LIBRARY_PATH ${thisdir}/lib64:${thisdir}/lib:${LD_LIBRARY_PATH}
else
   setenv LD_LIBRARY_PATH ${thisdir}/lib64:${thisdir}/lib
endif
if ($?PYTHONPATH) then
   setenv PYTHONPATH ${thisdir}/lib:${thisdir}/lib/python2.7/site-packages:${PYTHONPATH}
else
   setenv PYTHONPATH ${thisdir}/lib:${thisdir}/lib/python2.7/site-packages
endif
if ($?CMAKE_PREFIX_PATH) then
   setenv CMAKE_PREFIX_PATH ${thisdir}:${CMAKE_PREFIX_PATH}
else
   setenv CMAKE_PREFIX_PATH $thisdir
endif
#---then ROOT
if ( -e $thisdir/bin/root ) then
   set rootdir=`readlink $thisdir/bin/root`
   set rootdir=`dirname $rootdir`
   set rootdir=`dirname $rootdir`
   setenv ROOTSYS $rootdir
   if ($?ROOT_INCLUDE_PATH) then
      setenv ROOT_INCLUDE_PATH ${thisdir}/include:${ROOT_INCLUDE_PATH}
   else
      setenv ROOT_INCLUDE_PATH ${thisdir}/include
   endif   
endif
"""
    f = open(os.path.join(view_root,'setup.sh'), 'w')
    f.write(setup_sh.replace('@date@',time.strftime("%c")).replace('@compilerlocation@',compiler))
    f.close()
    f = open(os.path.join(view_root,'setup.csh'), 'w')
    f.write(setup_csh.replace('@date@',time.strftime("%c")).replace('@compilerlocation@',compiler))
    f.close()

def main():
    helpstring = """{0} [options] <view_destination>

This package creates a "view" of LCG release in folder <view_destination>.
"""

    # lcg_root = '/afs/cern.ch/sw/lcg/releases'
    # lcg_release = 79
    # lcg_platform = 'x86_64-slc6-gcc49-opt'
    # view_root = '/tmp/view_{0}{1}'.format(lcg_release, lcg_platform)

    parser = argparse.ArgumentParser(usage=helpstring.format(sys.argv[0]))
    parser.add_argument('view_destination', metavar='view_destination', nargs='+', help=argparse.SUPPRESS)
    parser.add_argument('-l', '--lcgpath', help="top directory of LCG releases (default: /afs/cern.ch/sw/lcg/releases)",
                        action="store",
                        default='/afs/cern.ch/sw/lcg/releases', dest='lcgpath')
    parser.add_argument('-r', '--release', help="LCG release number (default: 80)", action="store", default=80, dest="lcgrel")
    parser.add_argument('-p', '--platform', help="Platform to use (default: x86_64-slc6-gcc49-opt)", action="store",
                        default='x86_64-slc6-gcc49-opt', dest='lcgplat')
    parser.add_argument('-d', '--delete', help="delete old view before creating a new one", action="store_true",
                        default=False, dest='delview')
    parser.add_argument('-B', '--enable-blacklists', help='Enable built-in blacklists of files and packages. For experts only.',
                        action="store_true", default=False, dest='bl_enabled') 
    parser.add_argument('-v', '--version', action='version', version='%(prog)s 0.3')
    args = parser.parse_args()

    lcg_root = args.lcgpath
    lcg_release = args.lcgrel
    lcg_platform = args.lcgplat
    view_root = args.view_destination[0]

    if args.bl_enabled:
        blacklist = ['version.txt', '.filelist', 'README',
                     'LICENSE', 'decimal.h', 'atan2.h', 'project.cmt', 'INSTALL']
        greylist = ['site.py', 'site.pyc', 'easy_install', 'easy_install-2.7', 'setuptools.pth']
        pkg_blacklist = ['neurobayes_expert', 'vdt', 'cmt', 'Qt5', 'xrootd_python', 'powheg-box', 'sherpa-mpich2', 'yamlcpp']
    else:
        blacklist = ['version.txt']
        greylist  = []
        pkg_blacklist = []

    topdir_whitelist = ['aclocal', 'cmake', 'emacs', 'fonts', 'include', 'macros', 'test', 'tests', 
                        'bin', 'config', 'etc', 'icons', 'lib', 'lib64', 'man', 'tutorials', 'share']
    concatenatelist = ['easy-install.pth']

    if args.delview and os.path.exists(view_root):
        shutil.rmtree(view_root, True)

    if not os.path.exists(view_root):
        os.makedirs(view_root)


    #---Check whether the LCG release actually exists, otherwise take all the packages in the root directory
    release_root = os.path.join(lcg_root, 'LCG_%s' % lcg_release)
    if os.path.exists(release_root) :
        lcg_file = os.path.join(release_root, 'LCG_externals_%s.txt' % lcg_platform)
        mc_file = os.path.join(release_root, 'LCG_generators_%s.txt' % lcg_platform)
        externals = get_externals_from_release_file(lcg_file)
        externals.update(get_externals_from_release_file(mc_file))
    else :
        release_root = lcg_root 
        externals = get_externals_from_directory(release_root, lcg_platform)

    #---Loop over all packages-----------------------------------------------------------------------------------
    for pkg in externals:
        if pkg in pkg_blacklist:
            continue

        pkg_root = os.path.realpath(os.path.join(release_root, externals[pkg]['HOME']))
        print 'Package {0}: root = {1}'.format(pkg, pkg_root)

        for (dir_path, dirnames, filenames) in os.walk(pkg_root, followlinks=False):
            if 'doc' in splitall(dir_path):
                continue

            if 'logs' in splitall(dir_path):
                continue

            dirpath = dir_path.replace(pkg_root, '.')
            dirpath_s = splitall(dirpath)

            # Elinimate any top level file or directory not in the topdir_whitelist
            if len(dirpath_s) == 1 or dirpath_s[1] not in topdir_whitelist :
                continue 

            view_dir = os.path.realpath(os.path.join(view_root, dirpath))

            if not os.path.exists(view_dir):
                # print 'Create directory', os.path.realpath(os.path.join(view_root, dirpath))
                try:
                    os.makedirs(view_dir)
                except OSError as e:
                    if e.errno == 20:
                        print "*** WARNING: Target already exisits and is file: {0} ***".format(view_dir)
                        print "*** NOTICE: Added from: {0} ***".format(os.path.realpath(view_dir))
                        print "*** NOTICE: Conflicts with: {0} ***".format(os.path.realpath(dir_path))
                    else:
                        raise e

            # continue
            for f in filenames:
                if f in blacklist or f.startswith('.') or f.endswith('-env.sh') or f.endswith('~'):
                    continue

                source = os.path.join(dir_path, f)
                target = os.path.join(view_dir, f)

                source_rel = os.path.realpath(source).replace(lcg_root + os.path.sep, '')
                target_rel = os.path.realpath(target).replace(lcg_root + os.path.sep, '')
                
                if f in concatenatelist:
                    open(target,'a').write(open(source,'r').read())
                    continue

                if not os.path.exists(target):
                    # print "Create symlink: {0} -> {1}".format(source, target)
                    try:
                        os.symlink(source, target)
                    except OSError as e:
                        if e.errno == 20:
                            print "*** WARNING: Target already exisits and is file: {0} ***".format(f)
                            print "*** NOTICE: Added from: {0} ***".format(target_rel)
                            print "*** NOTICE: Conflicts with: {0} ***".format(source_rel)
                        else:
                            raise e
                else:
                    if f not in greylist: 
                        print "*** WARNING: File already exisits: {0} ***".format(target)
                        print "*** NOTICE: Added from: {0} ***".format(target_rel)
                        print "*** NOTICE: Conflicts with: {0} ***".format(source_rel)
                        #return 1
    
    #---Finalize the view with additional operations------------------------------------------------------------                      
    write_setup(lcg_root, view_root, lcg_platform)
    return 0

if __name__ == '__main__':
    exit(main())
