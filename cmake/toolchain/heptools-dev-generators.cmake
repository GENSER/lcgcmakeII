set(MCGENPATH  MCGenerators)


LCG_external_package(lcgenv         1.3.2.1                              )
LCG_external_package(heputils       1.1.0          ${MCGENPATH}/heputils )
LCG_external_package(heputils       1.0.6          ${MCGENPATH}/heputils )

LCG_external_package(mcutils        1.2.0          ${MCGENPATH}/mcutils  heputils=1.1.0)
LCG_external_package(mcutils        1.1.1          ${MCGENPATH}/mcutils heputils=1.0.6)

LCG_external_package(madgraph5amc       2.2.2          ${MCGENPATH}/madgraph5amc )

LCG_external_package(lhapdf            5.9.1          ${MCGENPATH}/lhapdf       )
LCG_external_package(lhapdf            6.1.5          ${MCGENPATH}/lhapdf       )
LCG_external_package(lhapdf            6.1.5.cxxstd   ${MCGENPATH}/lhapdf author=6.1.5 usecxxstd=1 )

LCG_external_package(lhapdfsets        5.9.1          lhapdfsets   )

LCG_external_package(powheg-box-v2         r3043.lhcb    ${MCGENPATH}/powheg-box-v2 author=r3043  )
LCG_external_package(powheg-box-v2     r3043.lhcb.rdynamic    ${MCGENPATH}/powheg-box-v2 author=r3043  )
LCG_external_package(powheg-box        r2092         ${MCGENPATH}/powheg-box       )

LCG_external_package(feynhiggs         2.10.2         ${MCGENPATH}/feynhiggs    )
LCG_external_package(chaplin           1.2            ${MCGENPATH}/chaplin      )

LCG_external_package(pythia8           175            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           185            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           186            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           201            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           205            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           209            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           210            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           212            ${MCGENPATH}/pythia8 )
LCG_external_package(pythia8           215            ${MCGENPATH}/pythia8 )

LCG_external_package(sacrifice         0.9.9          ${MCGENPATH}/sacrifice pythia8=186)
LCG_external_package(thepeg            1.9.2p1        ${MCGENPATH}/thepeg author=1.9.2 )

LCG_external_package(herwig++          2.7.1          ${MCGENPATH}/herwig++  thepeg=1.9.2p1 )

LCG_external_package(tauola++          1.1.1a         ${MCGENPATH}/tauola++     )
LCG_external_package(tauola++          1.1.4          ${MCGENPATH}/tauola++     )
LCG_external_package(tauola++          1.1.5          ${MCGENPATH}/tauola++     )

LCG_external_package(pythia6           427.2          ${MCGENPATH}/pythia6    author=6.4.27 hepevt=10000  )
LCG_external_package(pythia6           428.2          ${MCGENPATH}/pythia6    author=6.4.28 hepevt=10000  )
LCG_external_package(pythia6           429.2          ${MCGENPATH}/pythia6    author=6.4.28 hepevt=10000  )

LCG_external_package(agile             1.4.1          ${MCGENPATH}/agile        )

LCG_external_package(photos++          3.52           ${MCGENPATH}/photos++     )
LCG_external_package(photos++          3.56           ${MCGENPATH}/photos++     )
LCG_external_package(photos++          3.60           ${MCGENPATH}/photos++     )
LCG_external_package(photos++          3.61           ${MCGENPATH}/photos++     )

LCG_external_package(photos            215.4          ${MCGENPATH}/photos       )

LCG_external_package(evtgen            1.2.0          ${MCGENPATH}/evtgen       tag=R01-02-00 pythia8=175 tauola++=1.1.1a)
LCG_external_package(evtgen            1.3.0          ${MCGENPATH}/evtgen       tag=R01-03-00 pythia8=186 tauola++=1.1.4)
LCG_external_package(evtgen            1.4.0          ${MCGENPATH}/evtgen       tag=R01-04-00 pythia8=186 tauola++=1.1.4)
LCG_external_package(evtgen            1.5.0          ${MCGENPATH}/evtgen       tag=R01-05-00 pythia8=212 tauola++=1.1.5)

LCG_external_package(rivet             2.4.0          ${MCGENPATH}/rivet        yoda=1.5.5      )

LCG_external_package(sherpa            2.1.1          ${MCGENPATH}/sherpa       author=2.1.1 hepevt=10000)

LCG_external_package(qd                2.3.13         ${MCGENPATH}/qd          )

if (NOT ${LCG_HOST_ARCH} STREQUAL i686)
LCG_external_package(blackhat           0.9.9         ${MCGENPATH}/blackhat    )
LCG_external_package(sherpa-mpich2     2.1.1          ${MCGENPATH}/sherpa-mpich2  author=2.1.1 hepevt=10000)
LCG_external_package(sherpa            2.2.0.blackhat ${MCGENPATH}/sherpa         author=2.2.0 hepevt=10000 lhapdf=6.1.5)
endif()

LCG_external_package(hepmcanalysis     3.4.14         ${MCGENPATH}/hepmcanalysis  author=00-03-04-14  CLHEP=2.3.1.1      )
LCG_external_package(mctester          1.25.0         ${MCGENPATH}/mctester     )
LCG_external_package(hijing            1.383bs.2      ${MCGENPATH}/hijing       )
LCG_external_package(starlight         r43            ${MCGENPATH}/starlight    )

LCG_external_package(herwig            6.520.2        ${MCGENPATH}/herwig       )
LCG_external_package(herwig            6.521.2        ${MCGENPATH}/herwig       )

LCG_external_package(crmcold           v3400          ${MCGENPATH}/crmc         )
LCG_external_package(crmc              1.4            ${MCGENPATH}/crmc         )
LCG_external_package(crmc              1.5.3          ${MCGENPATH}/crmc         )
LCG_external_package(crmc              1.5.4          ${MCGENPATH}/crmc         )

LCG_external_package(cython            0.19.1         ${MCGENPATH}/cython       )
LCG_external_package(cython            0.22           ${MCGENPATH}/cython       )

LCG_external_package(yoda              1.5.5          ${MCGENPATH}/yoda  cython=0.22     )

LCG_external_package(hydjet            1.6            ${MCGENPATH}/hydjet author=1_6 )
LCG_external_package(hydjet            1.8            ${MCGENPATH}/hydjet author=1_8 )
LCG_external_package(tauola            28.121.2       ${MCGENPATH}/tauola       )

LCG_external_package(jimmy             4.31.3         ${MCGENPATH}/jimmy        )
LCG_external_package(hydjet++          2.1            ${MCGENPATH}/hydjet++ author=2_1)
LCG_external_package(alpgen            2.1.4          ${MCGENPATH}/alpgen author=214 )
LCG_external_package(pyquen            1.5.1          ${MCGENPATH}/pyquen author=1_5)
LCG_external_package(baurmc            1.0            ${MCGENPATH}/baurmc       )
LCG_external_package(professor         1.4.0          ${MCGENPATH}/professor    )
LCG_external_package(professor         2.0.0          ${MCGENPATH}/professor    )

LCG_external_package(jhu               5.6.3          ${MCGENPATH}/jhu          )

LCG_external_package(vincia            1.1.03          ${MCGENPATH}/vincia pythia8=185)
LCG_external_package(vincia            1.2.00          ${MCGENPATH}/vincia pythia8=205)
LCG_external_package(vincia            1.2.01          ${MCGENPATH}/vincia pythia8=205)

LCG_external_package(fastnlo_toolkit   2.3.1pre-1871  ${MCGENPATH}/fastnlo_toolkit )

