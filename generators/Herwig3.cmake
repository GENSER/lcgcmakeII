# TODO check -O compile options
# for many packages -O0 is used (

LCGPackage_Add(
  FORM
  URL ${gen_url}/FORM-v<NATIVE_VERSION>.tar.gz
  CONFIGURE_COMMAND autoreconf -i
            COMMAND ./configure --prefix=<INSTALL_DIR>
  BUILD_IN_SOURCE 1
)

# TODO cannot link feynhiggs
LCGPackage_Add(
  vbfnlo
  URL ${gen_url}/vbfnlo-<NATIVE_VERSION>.tgz
  CONFIGURE_COMMAND ./configure --prefix=<INSTALL_DIR> 
                    --with-hepmc=${HepMC_home}
                    --with-gsl=${GSL_home}
                    --with-LHAPDF=${lhapdf_home}
                    --with-LOOPTOOLS=${looptools_home}
#                    --with-FEYNHIGGS=<feynhiggs-<vbfnlo_<VERSION>_feynhiggs>_home>
  BUILD_IN_SOURCE 1
  DEPENDS HepMC GSL lhapdf looptools feynhiggs-<vbfnlo_<VERSION>_feynhiggs>
)

## TODO: clean from waste in INSTALL DIR
LCGPackage_Add(
  openloops
  URL ${gen_url}/OpenLoops-<NATIVE_VERSION>.tar.gz
  CONFIGURE_COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/generators/openloops.cfg ./
            COMMAND ./scons
  BUILD_COMMAND ./openloops libinstall ppll
  INSTALL_COMMAND ${CMAKE_COMMAND} -DSRC=<SOURCE_DIR> -DDST=<INSTALL_DIR> -P ${CMAKE_SOURCE_DIR}/cmake/scripts/copy.cmake
  BUILD_IN_SOURCE 1
)

# is vc package needed?
LCGPackage_Add(
  njet
  URL ${gen_url}/njet-<NATIVE_VERSION>.tar.gz
  CONFIGURE_COMMAND ./configure --prefix=<INSTALL_DIR>
                    --disable-autoflags
                    --with-qd=${qd_home} FFLAGS=-ffixed-line-length-none FC=${CMAKE_Fortran_COMPILER} CC=${CMAKE_C_COMPILER} F77=${CMAKE_Fortran_COMPILER} 
  BUILD_IN_SOURCE 1
  DEPENDS qd
)

# TODO clean from waste
LCGPackage_Add(
  qgraf
  URL ${gen_url}/qgraf-3.1.4.tgz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND $ENV{FC} -o qgraf qgraf-3.1.4.f
  INSTALL_COMMAND ${CMAKE_COMMAND} -E make_directory <INSTALL_DIR>/bin
          COMMAND ${CMAKE_COMMAND} -DSRC=<SOURCE_DIR> -DDST=<INSTALL_DIR>/bin -P ${CMAKE_SOURCE_DIR}/cmake/scripts/copy.cmake
  BUILD_IN_SOURCE 1
)

# TODO ?looptools, 
LCGPackage_Add(
  gosam_contrib
  URL ${gen_url}/gosam-contrib-<NATIVE_VERSION>.tar.gz
  CONFIGURE_COMMAND ./configure --prefix=<INSTALL_DIR>
#                    --with-looptools=${looptools_home}
  BUILD_COMMAND ${MAKE}
  BUILD_IN_SOURCE 1
#  DEPENDS looptools
)

LCGPackage_Add(
  gosam
  URL ${gen_url}/gosam-<NATIVE_VERSION>.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND ${Python_cmd} setup.py build
  INSTALL_COMMAND ${Python_cmd} setup.py install --prefix=<INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS gosam_contrib qgraf FORM Python
)


#- Herwig3 ----------------------
set(thepeg3_v_home <thepeg-<herwig3_<VERSION>_thepeg>_home>)
LCGPackage_Add(
    herwig3
    URL ${gen_url}/Herwig-<NATIVE_VERSION>.tar.bz2
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory <INSTALL_DIR>/tmppdfsets
    COMMAND ${lhapdf_home}/bin/lhapdf --pdfdir=<INSTALL_DIR>/tmppdfsets install MMHT2014lo68cl MMHT2014nlo68cl
    COMMAND ./configure --prefix=<INSTALL_DIR>
                                  --with-gsl=${GSL_home}
                                  --with-thepeg=${thepeg3_v_home}
                                  --with-thepeg-headers=${thepeg3_v_home}/include
                                  --with-fastjet=${fastjet_home}
                                  --with-boost=${Boost_home}
                                  --with-madgraph=${madgraph5amc_home}
                                  --with-openloops=${openloops_home}
                                  --with-gosam-contrib=${gosam_contrib_home}
                                  --with-gosam=${gosam_home}
                                  --with-njet=${njet_home}
                                  --with-vbfnlo=${vbfnlo_home}
                                  ${library_path}=${lhapdf_home}/lib:${GSL_home}/lib:$ENV{${library_path}}
    BUILD_COMMAND ${MAKE} all ${library_path}=${lhapdf_home}/lib:${thepeg3_v_home}/lib/ThePEG:${GSL_home}/lib:${Boost_home}/lib:${fastjet_home}/lib:$ENV{${library_path}} LIBRARY_PATH=${fastjet_home}/lib
    INSTALL_COMMAND ${MAKE} install ${library_path}=${lhapdf_home}/lib:${thepeg3_v_home}/lib/ThePEG:${GSL_home}/lib:${Boost_home}/lib:${HepMC_home}/lib:${fastjet_home}/lib:$ENV{${library_path}} LIBRARY_PATH=${fastjet_home}/lib:${thepeg3_v_home}/lib/ThePEG:${lhapdf_home}/lib LHAPDF_DATA_PATH=<INSTALL_DIR>/tmppdfsets
            COMMAND ${CMAKE_COMMAND} -E remove -f <INSTALL_DIR>/tmppdfsets
    BUILD_IN_SOURCE 1
    DEPENDS lhapdf Boost Python GSL thepeg-<herwig3_<VERSION>_thepeg> fastjet vbfnlo openloops madgraph5amc njet gosam
  )

foreach(v ${herwig3_native_version})
set (vv "")
foreach(herwigtest LHC-Matchbox-MadGraph-MadGraph LHC-Matchbox-MadGraph-OpenLoops LHC-Matchbox-MadGraph-GoSam)
LCG_add_test(herwig3-${v}.${herwigtest} 
                          TEST_COMMAND $ENV{SHELL} -c "${lcgenv_home}/lcgenv -p ${CMAKE_INSTALL_PREFIX} ${LCG_platform} herwig3 ${v} > h3-${v}.env
                            source h3-${v}.env
                            rm -rf Herwig-scratch
                            ${herwig3-${v}_home}/bin/Herwig build ${CMAKE_SOURCE_DIR}/generators/Herwig3/${herwigtest}.in 
                            ${herwig3-${v}_home}/bin/Herwig integrate LHC-Matchbox.run"
                          POST_COMMAND cat Herwig-scratch/Build/MadGraphAmplitudes/MG.log
                          ENVIRONMENT LHAPDF_DATA_PATH=${herwig3-${v}_home}/tmppdfsets 
                          ${vv}
                          )
set (vv DEPENDS herwig3-${v}.${herwigtest})
endforeach()
endforeach()


