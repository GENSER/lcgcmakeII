#!/usr/bin/env python

from os import listdir
from os.path import isfile, join
import shutil
import os
from optparse import OptionParser
from common_parameters import officialarea, tmparea
import sys, errno
import Tools

def copyRPMS(localarea, remotearea, target="all", policy="Release"):
    buildarea = join(localarea,"packaging/rpmbuild/RPMS/noarch/") 
    existingrpms = set()
    for root, dirs, files in os.walk(officialarea, topdown=False):
      new = set( f.split("-1.0.0")[0] for f in files if isfile(join(root,f)) )
      existingrpms.update(new)
    newrpms   = set( f for f in listdir(buildarea) if isfile(join(buildarea,f)) )

    if policy in ("Generators","Release"):
      # only copy RPMS of new packages
      #  don't overwrite previous RPM revisions
      for item in newrpms:
        if item.split("-1.0.0")[0] not in existingrpms:
          shutil.copy2(join(buildarea,item), remotearea)
          print "Copied %s to %s" %(item, remotearea)
      # in case we upgrade generators we have to explicitly override LCG_generators_XYZ_ and LCGMeta_XYZ_generators
      if policy=="Generators" or target=="generators":
        for item in newrpms:
          if (item.startswith("LCGMeta") and "generators" in item) or item.startswith("LCG_generators"):
            shutil.copy2(join(buildarea,item), remotearea)
            print "Copied %s to %s" %(item, remotearea)    
    elif policy == "Rebuild":
      # force overwriting a single package
      targetname = target.split("-")[0] # remove version from <package>-<version> cases like rivet-2.3.0
      rpms_to_copy = [f for f in newrpms if (f.lower().split("-")[0] == targetname.lower() ) ]
      if len(rpms_to_copy) != 1:
        print "ERROR: no RPM for package %s found" %target
        sys.exit(errno.EINVAL)
      else:
        item = rpms_to_copy[0]
        shutil.copy2(join(buildarea,item), remotearea)
        print "Copied %s to %s" %(item, remotearea)        
    else:
      print "ERROR: policy %s and target %s invalid!" %(policy, target)
      sys.exit(errno.EINVAL)

##########################
if __name__ == "__main__":

  # extract command line parameters
  usage = "usage: %prog localarea target\n  target is either 'all', 'generators' or a specific one like 'Qt'"
  parser = OptionParser(usage)
  (options, args) = parser.parse_args()
  if len(args) != 2:
      parser.error("incorrect number of arguments.")
  else:
      localarea = args[0]   #where RPMS have been build
      target    = args[1]

  copyRPMS(localarea,tmparea,target=target)
