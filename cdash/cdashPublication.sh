#!/bin/bash -x
TIMESTAMP="$(find /build/jenkins/workspace/${JOB_NAME%%/*}\/BUILDTYPE/$BUILDTYPE/COMPILER/$COMPILER/LABEL/$LABEL/*/Testing -maxdepth 1 -mindepth 1 -type d -name '2*' -printf "%f")"
BUILDNAME="$(find /build/jenkins/workspace/${JOB_NAME%%/*}\/BUILDTYPE/$BUILDTYPE/COMPILER/$COMPILER/LABEL/$LABEL/ -maxdepth 1 -mindepth 1 -type d -name '*build*' -printf "%f")"
BUILDNAME=${BUILDNAME%-build}

echo HERE IS THE CDASH URL: https://cdash.cern.ch/index.php?project=LCGSoft\&filtercount=3\&showfilters=1\&filtercombine=and\&field1=buildname/string\&compare1=61\&value1=$SLOTNAME\-$BUILDNAME\&field2=site/string\&compare2=61\&value2=$NODE_NAME\.cern.ch\&field3=buildstamp/string\&compare3=61\&value3=$TIMESTAMP\-$MODE\&collapse=0

