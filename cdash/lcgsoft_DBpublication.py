#!/usr/bin/env python
import os,sys
import glob
import subprocess

__release_area__ = "/afs/cern.ch/sw/lcg/releases/"

parameters = sys.argv
if len(parameters) != 1:
    print "PArameters to provide: LCG_VERSION"
    sys.exit()

lcg_version = parameters[1]

release_directory = os.path.join(__release_area__,"LCG_%s"%lcg_version)

platform_list = []

for name in glob.glob(release_directory+"/LCG_externals*.txt"):
    file_name = os.path.basename(name)
    tmp1 = file_name.split('externals_')
    platform = tmp1[1].split('.txt')
    platform_list.append(platform[0])
    command = "/afs/cern.ch/sw/lcg/app/spi/www/lcgsoft/public_html/lcgsoft/scripts/ReleaseSummaryReader_orig %s %s" %(lcg_version,platform[0])
    print subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read()    


